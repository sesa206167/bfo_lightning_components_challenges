# README #

There is a lot of buzz about lightning components and they talk about complex javascript frameworks which work well together to create complex applications . Its fairly simple and the way we can strengthen our skills is by building simplest applications without poluting our knowledge by googling around too much or be impressed by the UI affects we see in lot of framework examples. 

I would like to give a simplest introduction to Lightning Components and feel free to correct me or add to this readme , would be happy to learn from you.

Lets forget about complex frameworks like jquery, angular, react and lets try to focus on the basic vocabulary of javascript.

just like any language we have basic structure . you have reserved words which help you create statements and have some grammar so the browser can understand what you are trying to say. 

if I summarize we just have 
##### variables, constants, enums #####
```
var x= 1;
var y='sample string';
var z={'name':'somename'};
// we have basic types we construct complex types with those.
```
##### conditional statements #####
```
if(x===1)
// or
x===1?console.log('1'):console.log('nothing');
```
##### iteration statements #####
```
for(i=0;i<10;i++) console.log(i);
[1,2,3].forEach(function(item){console.log(item)});
// just a different syntax to iterate and display or do something with variables.
```
##### functions #####
we group a bunch of statements to use them multiple times or just for readability. 

##### events#####
you create a event and subscribe to event , just like our real world examples : take an example of you recieving a call on your phone. When your phone recieves a call some part of the phone fires up an event 'incoming call', and the ringer just sings a song or a ringtone because its listening to it. we would use the same mechanism to communicate between independent components. A notification component on phone can listen to that and blink led lights. So we can keep on adding more components that can listen to an event 'incoming call'.


Yeah I know the events part seems more complex , but believe me its not, we need to understand that its like a radio, you broadcast an event and all interested parties can listen to it and behave in a specific way.

### Interactions with Front end ###
We will use HTML + CSS to design our front end.

HTML - simple markup language which browsers can understand to display the front end. We have a lot of vocabulary in HTML but we can design most of them with basic tags, h1,p,span,div,table,ul,li. 
CSS helps you style your HTML document so it looks more readable . 

We usually spend a lot of time in styling the document to work with all the browsers and all sizes of windows and they have to support mobile and stuff so we rely on the css frameworks which would give us a bunch of classes to make our life easier. 

a class is a group of css statements that we can apply to any html element. 

We have the [Lightning design system ](http://lightningdesignsystem.com) which has good collection of classes, take your time to read through the guidelines. Dont rush into copy pasting the code to see the output. If you are not comfortable to play with CSS classes of lightning you would end up complicating your application and when you look back you see a huge bunch of html code and patch works of CSS you introduce by googling. So take your time with this.

Javascript is a scripting language , mainly designed to add dynamic functionality to a simple HTML document. 

The dynamic functionality can be showing or hiding elements , modifying some style properties, and modify some data around .

### How do we get data from Salesforce ###

Primarily from apex. No matter where the data is stored , its easier to ensure that the data is coming from apex.
just use the @AuraEnabled annotation for any static function and it can be your data source for a lightning component that has the class as a controller.

ex: 
```
public with sharign class myComponentController{
	@AuraEnabled
	public static List<Contact> getSomeContacts(){
		return [Select Id,Name from Contact limit 10];
	}
}
```
MyComponent.cmp
```
<aura:component  controller="myComponentController">
	<aura:handler name="init" value="{!this}" action="{!c.doInit}" />
	<aura:iteration items="{!v.mycontacts}" var="contact">
		<ul>
			<li>{!contact.Name}</li>
		</ul>	
	</aura:iteration>
</aura:component>
```

As mentioned above the handler is just to handle a event, and thats the syntax to handle a simple initialization event and call a function from controller. It looks a bit cryptic as why we are passing the value as {!this} etc, but its just a cryptic syntax salesforce decided. You can dig deeper if you like, but I would keep it simple for our understanding  .

We need a function to initialize our component and thats the syntax.

MyComponentController.js
```
doInit:function(component,event,helper){
	var action = component.get('c.getSomeContacts');
	action.setCallback(this,function(response){
		var contacts = response.getReturnValue();
		component.set('v.contacts',contacts);
	})
}
```

we have a few variables that are passed around Component and event .Component has some functions to identify the items on your front end and also on your apex class (just the functions you exposed with @AuraEnabled) . So any functions that we have are referred with c. (my guess is controller) and front end variables are referred to with v. (and its the view) syntax. 


### Challenge Rules ###

* No additional javascript frameworks to be used, angular, jquery,react, not even underscorejs. just plain simple javascript . Even if its more code you have to write and it looks messy.
* LightningDesignSystem for styling , no bootstrap or inline css lines. all the components that the challenge requires have classes inside lightning design system. So take time and read through the documentation of lightning design system. 
* Document your functions - if you don't we are not going to read them.

### Challenge Components ###

* Build a calander component to display Events from Salesforce. ex: input - list<Event>.
* Build a table of Opportunities with atleast 5 fields which supports sorting on multiple columns.
* Build a autocomplete search box to fill the values of a picklist field.

### Evaluation guidelines ###

* Component that follows Lightning UX + Subsequent scalability or extension of the component to add more features would be considered a winner.

### How to Submit ###
* Fork this repository 
* Create your components in your developer organization, keep the auradefinitionbundle and classes associated with your development in the src/aura and src/classes folder.
* create a pull-request for the repository and submit. 

### Who do I talk to? ###

* Sid
* Ram